<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('company_name')->nullable()->default(null);
            $table->string('firstname')->nullable()->default(null);
            $table->string('lastname')->nullable()->default(null);
            $table->string('patronymic')->nullable()->default(null);
            $table->string('fullname')->nullable()->default(null);
            $table->string('postcode')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('phone_work')->nullable()->default(null);
            $table->string('phone_personal')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone_personal');
            $table->dropColumn('phone_work');
            $table->dropColumn('city');
            $table->dropColumn('description');
            $table->dropColumn('postcode');
            $table->dropColumn('fullname');
            $table->dropColumn('patronymic');
            $table->dropColumn('lastname');
            $table->dropColumn('firstname');
            $table->dropColumn('company_name');
        });
    }
}
