<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'id' => 1,
        	'name' => 'Марина',
        	'email' => 'info@yan-berg.ru',
        	'password' => Hash::make('2NYyh1ax'),
        ]);
    }
}
