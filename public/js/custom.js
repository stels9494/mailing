
function agreeRemove($name) {
    return confirm('Вы действительно хотите безвозвратно удалить "'+$name+'" ?');
}

function getUserFields($url) {
    $.ajax({
        url: $url,
        type: 'get',
        success: function (data) {
            $(data.data).each(function (i, v) {
                $('.relation #to').append('<option value="'+v+'">'+v+'</option>');
            });
            $('.import-check').css('display', 'none');
            $('.relations').css('display', 'block');
            // отобразить селекты
            
        },
        error: function (data) {
            console.log(data);
        }
    })
}

function validateMailingForm(t) {
    console.log(t);
    var select = t.find('select[name=template_id]');
    if (!select.val()) {
        select.addClass('is-invalid');
        return false;
    }
    return true;
}

function removeIsInvalidClass(t) {
    t.removeClass('is-invalid');
}

$(document).ready(function () {
    // $('textarea#template').ckeditor();

    $('.add-more-files').click(function (e) {
        e.preventDefault();
        $('.buffer.file > div').clone(true).appendTo($('.files .form-group:last-child').parent());
    });

    $('.remove-input-file').click(function (e) {
        e.preventDefault();
        // console.log($(this).closest('.form-group').find('input').prop('files'));
        $(this).closest('.form-group').remove();
    });
    
    $('.clear-input-file').click(function (e) {
        e.preventDefault();
        console.log($(this).closest('.form-group').find('input[type="file"]').val(""));
    });

    $('.remove-attach-file').click(function (e) {
        e.preventDefault();
        console.log($(this).closest('.attach-file').data('removeUrl'));
        var t = $(this);
        $.ajax({
            url: $(this).closest('.attach-file').data('removeUrl'),
            type: 'delete',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'action': 'remove-file',
                'file_id': $(this).closest('.attach-file').data('fileId'),
            },
            success: function (data) {
                t.closest('.attach-file').remove();
            }
        });
        console.log('asdfasd');
    });

    $('.template-remove, .user-remove').click(function (e) {
        e.preventDefault();
        console.log('submit');
        $(this).closest('form').submit();
    })

    $('.import-check').click(function (e) {
        e.preventDefault();
        var file = $(this).closest('form').find('input[type="file"]').prop('files');
        if (!file.length) {
            alert('Файл не выбран');
            return;
        }
        var form = new FormData;
        var t = $(this);
        form.append('file', file[0]);
        form.append('delimeter', $('input[name="delimeter"]').val());
        $.ajax({
            url: $(this).data('checkImportUrl'),
            type: 'POST',
            processData: false,
            contentType: false,
            data: form,
            success: function (data) {
                $(data.data).each(function (i, v) {
                    $('.relation #from').append('<option value="'+v+'">'+v+'</option>');
                });
                // запросить поля бд
                getUserFields(t.data('getUserFieldsUrl'));

            },
            error: function (data) {
                alert('Произошла ошибка.', data);
            }

        })
    });

    $('.relation-add').click(function (e) {
        e.preventDefault();
        var relations = $('.relations-container .relation');
        if (relations.length < relations.first().find('#to option:nth-child(n+2)').length) {
            var i = $('.relations-container .relation:last-child').data('i');
            i++;
            $('.buffer.relation > div').clone(true).appendTo($('.relations-container'));
            var lastRelation = $('.relations-container .relation:last-child');
            lastRelation.data('i', i);
            var nameTo = 'relations['+i+'][to]';
            var nameFrom = 'relations['+i+'][from]';
            lastRelation.find('select#to').attr('name', nameTo);
            lastRelation.find('select#from').attr('name', nameFrom);
        }       
    });
    
    // выделить всех пользователей на странице
    $('.users-checkbox').change(function () {    
        $('.user-checkbox').prop('checked', $(this).prop('checked'));
    });

    $('.run-mailing').click(function () {
        var users = $('.user-checkbox:checked');
        usersJson = {};
        users.each(function (i, user) {
            usersJson[i] = $(user).data('userId');
        });
        $('input[name=users]').val(JSON.stringify(usersJson));
    });

    // меняет кол-во записей на странице
    $('.number-view').change(function () {
        console.log(location.origin+location.pathname+'?'+'qty='+$(this).val());
        location.href = location.origin+location.pathname+'?'+'qty='+$(this).val();
    });

    // закрытие сообщения
    $('.alert .close').click(function () {
        $(this).closest('.alert').remove();
    })

    $('.logout').click(function (e) {
        e.preventDefault();
        $(this).closest('form').submit();
    })

    var CSRFToken = $('meta[name="csrf-token"]').attr('content');
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='+CSRFToken,
        extraPlugins: 'justify,codesnippet'
    };
    CKEDITOR.replace('template', options);
});