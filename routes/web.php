<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('register', function () {
	abort(404);
});
Route::post('register', function () {
	abort(404);
});

Route::get('/', function () {
	return redirect()->route('templates.index');
});
Route::get('message.jpg', 'Controller@messageViewed')->name('message.view');

Route::middleware(['auth'])->group(function () {
	Route::resource('templates', 'TemplateController');
	Route::resource('users', 'UserController');
	Route::resource('import', 'ImportController')->only(['index', 'store']);
	Route::get('messages', 'MessageController@index')->name('messages.index');	
	Route::post('mailing', 'Controller@mailingRun')->name('mailing.run');
});
