@extends('layouts.mailing')

@section('title')
    Создание нового шаблона для письма
@endsection

@section('content')
    {{ Form::open(['url' => route('templates.store'), 'enctype' => 'multipart/form-data']) }}
    <div>
        <div class="form-group">
            {{ Form::label('name', 'Название шаблона', ['class' => 'label-control']) }}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Введите название']) }}
        </div>
        <div class="form-group">
            {{ Form::label('subject', 'Тема письма', ['class' => 'label-control']) }}
            {{ Form::text('subject', null, ['class' => 'form-control', 'id' => 'subject', 'placeholder' => 'Введите тему письма']) }}
        </div>
        <div class="form-group">
            {{ Form::label('template', 'Шаблон письма', ['class' => 'label-control']) }}
            <p>Список директив:
                @foreach($data['listDirectives'] as $directive)
                    <small><?='{{'.$directive.'}}'?></small>
                @endforeach
            </p>
            {{ Form::textarea('template', null, ['class' => 'form-control'])}}
        </div>
        <div class="files">
            <div class="form-group">
                    {{ Form::file('file[]') }}
                    <a class="clear-input-file trash" href="#"><i class="fa fa-trash"></i></a>
            </div>            
        </div>
        <div class="row">
            <div class="form-group">
                <a title="Еще файл" class="add-more-files" href="#"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
    <div class="form-group text-center">
            {{ Form::submit('Создать', ['class' => 'btn btn-primary']) }}
            {{ Html::tag('a', 'Назад', ['href' => route('templates.index'), 'class' => 'btn btn-outline-secondary']) }}        
    </div>
    {{ Form::close() }}
    <div class="buffer file">
        <div class="form-group">
            {{ Form::file('file[]') }}
            <a class="remove-input-file trash" href="#"><i class="fa fa-trash"></i></a>
        </div>
    </div>
@endsection