@extends('layouts.mailing')

@section('title')
    Список всех шаблонных писем
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <a class="btn btn-primary" href="{{ route('templates.create') }}">Создать <i class="fa fa-plus"></i></a>
            </div>                    
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-2 mt-3 mb-3">
            <select class="number-view custom-select">
                @foreach ($data['qty'] as $one)
                    <option {{ $data['currentQty'] == $one ? 'selected' : '' }} value="{{ $one }}">{{ $one }}</option>
                @endforeach
            </select>
        </div>
    </div>
    {{ $data['templates']->links() }}
    <table class="table table-striped">
        <thead>
            <th class="w-25">#</th>
            <th class="w-50">Название</th>
            <th class="w-25">&nbsp;</th>
        </thead>
        <tbody>
            @foreach ($data['templates'] ?? [] as $template)
                <tr>
                    <td>{{ $template->id }}</td>
                    <td>{{ $template->name }}</td>
                    <td class="text-right">
                        <a title="Редактировать" href="{{ route('templates.edit', $template->id) }}"><i class="fas fa-edit"></i></a>
                        {{ Form::open(['url' => route('templates.destroy', $template), 'method' => 'delete', 'onsubmit' => 'return agreeRemove("'.$template->name.'")', 'class' => 'd-inline']) }}
                            <a class="trash template-remove" title="Удалить шаблон" href="#"><i class="fas fa-trash"></i></a>
                            <input type="hidden" name="action" value="remove-template">
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $data['templates']->links() }}
    
@endsection