@extends('layouts.mailing')

@section('title')
    Импорт пользователй из CSV
@endsection

@section('content')
    {{ Form::open(['url' => route('import.store'), 'enctype' => 'multipart/form-data']) }}
        <div class="form-group">
            <label for=""></label>
            {{ Form::file('file', ['accept' => '.xlsx,.xls']) }}
        </div>
        <div class="form-group">
            {{ Form::checkbox('replace', true, true, ['id' => 'replace', 'class' => 'ml-1 mr-1']) }}
            {{ Form::label('replace', 'Перезаписывать данные по совпадению email') }}
        </div>
        <div style="display: none;" class="form-group relations">
            <h3 class="text-center mb-3">Ключ - Значение</h3>
            <div class="relations-container">
                <div class="relation col-12" data-i="0">
                    <div class="row">
                        <div class="col-6">
                            <select class="form-control" name="relations[0][to]" id="to">
                                <option selected disabled>Значение пользователя</option>
                            </select>            
                        </div>
                        <div class="col-6">
                            <select class="form-control" name="relations[0][from]" id="from">
                                <option selected disabled>Значение файла</option>
                            </select>            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center">
                <a class="relation-add" href="#"><i class="fa fa-plus mt-4"></i></a>
            </div>
            <div class="col-12 text-center">
                {{ Form::submit('Импортировать', ['class' => 'btn btn-primary']) }}
            </div>
        </div>

        <a class="import-check btn btn-primary" href="#" data-check-import-url="{{ route('get-headers-excel') }}" data-get-user-fields-url="{{ route('get-user-fields') }}">Проверить</a>

    {{ Form::close() }}
    <div class="buffer relation">
        <div class="relation col-12 mt-2">
            <div class="row">
                <div class="col-6">
                    <select class="form-control" id="to">
                        <option selected disabled>Значение пользователя</option>
                    </select>            
                </div>
                <div class="col-6">
                    <select class="form-control" id="from">
                        <option selected disabled>Значение файла</option>
                    </select>            
                </div>
            </div>
        </div>
    </div>
@endsection
