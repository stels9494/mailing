<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
    <link rel="stylesheet" href="/css/custom.css">
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="{{ route('templates.index') }}">Mailing Panel</a>
{{--           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button> --}}
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item {{ Request::segment(1) === 'templates' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('templates.index') }}">Шаблоны <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item {{ in_array(Request::segment(1), ['users', 'import']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('users.index') }}">Пользователи</a>
              </li>
              <li class="nav-item {{ Request::segment(1) === 'messages' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('messages.index') }}">Статусы писем</a>
              </li>
            </ul>
            <div>
              <form action="{{ route('logout') }}" method="post">
                @csrf
                <a title="Выйти из системы" class="logout" href="#"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a>
              </form>
            </div>
          </div>
        </nav>
    </header>
    <div class="container">

        @if (session('success'))
          <div class="alert alert-success" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif

        @if (session('error'))
          <div class="alert alert-danger" role="alert">
            {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif

        @yield('content')
    </div>
</body>
<footer>
    
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="/js/custom.js"></script>
</footer>
</html>