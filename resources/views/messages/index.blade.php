@extends('layouts.mailing')

@section('title')
	
@endsection

@section('content')
    
    <div class="row">
        <div class="col-12 col-md-2 mt-3 mb-3">
            <select class="number-view custom-select">
                @foreach ($data['qty'] as $one)
                    <option {{ $data['currentQty'] == $one ? 'selected' : '' }} value="{{ $one }}">{{ $one }}</option>
                @endforeach
            </select>
        </div>
    </div>
    {{ $data['messages']->links() }}  
    <table class="table table-striped">
        <thead>
            <th>#</th>
            <th class="25">email</th>
            <th class="w-25">ФИО</th>
            <th class="w-50">Шаблон</th>
            <th class="">Прочитано</th>
        </thead>
        <tbody>
            @foreach ($data['messages'] ?? [] as $msg)
                <tr>
                    <td>{{ $msg->id }}</td>
                    <td>{{ $msg->user->email }}</td>
                    <td>{{ $msg->user->name }}</td>
                    <td>{{ $msg->template->name }}</td>
                    <td class="text-center">{{ $msg->viewed }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $data['messages']->links() }}    	
@endsection