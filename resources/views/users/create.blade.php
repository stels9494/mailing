@extends('layouts.mailing')

@section('title')
    Создание нового пользователя
@endsection

@section('content')
    {{ Form::open(['url' => route('users.store')]) }}
    <div>
        <div class="form-group">
            {{ Form::label('email', 'Email *', ['class' => 'label-control']) }}
            {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Введите Email']) }}
        </div>
        <div class="form-group">
            {{ Form::label('name', 'Название', ['class' => 'label-control']) }}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Введите название']) }}
        </div>


        <div class="form-group">
            {{ Form::label('position', 'Должность', ['class' => 'label-control']) }}
            {{ Form::text('position', null, ['class' => 'form-control', 'id' => 'position', 'placeholder' => 'Введите должность']) }}
        </div>

        <div class="form-group">
            {{ Form::label('company_name', 'Название компании', ['class' => 'label-control']) }}
            {{ Form::text('company_name', null, ['class' => 'form-control', 'id' => 'company_name', 'placeholder' => 'Введите название компании']) }}
        </div>

        <div class="form-group">
            {{ Form::label('firstname', 'Имя', ['class' => 'label-control']) }}
            {{ Form::text('firstname', null, ['class' => 'form-control', 'id' => 'firstname', 'placeholder' => 'Введите имя']) }}
        </div>

        <div class="form-group">
            {{ Form::label('lastname', 'Фамилия', ['class' => 'label-control']) }}
            {{ Form::text('lastname', null, ['class' => 'form-control', 'id' => 'lastname', 'placeholder' => 'Введите фамилию']) }}
        </div>

        <div class="form-group">
            {{ Form::label('patronymic', 'Отчество', ['class' => 'label-control']) }}
            {{ Form::text('patronymic', null, ['class' => 'form-control', 'id' => 'patronymic', 'placeholder' => 'Введите отчество']) }}
        </div>

        <div class="form-group">
            {{ Form::label('fullname', 'ФИО', ['class' => 'label-control']) }}
            {{ Form::text('fullname', null, ['class' => 'form-control', 'id' => 'fullname', 'placeholder' => 'Введите ФИО']) }}
        </div>

        <div class="form-group">
            {{ Form::label('postcode', 'Почтовый индекс', ['class' => 'label-control']) }}
            {{ Form::text('postcode', null, ['class' => 'form-control', 'id' => 'postcode', 'placeholder' => 'Введите почтовый индекс']) }}
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Описание', ['class' => 'label-control']) }}
            {{ Form::text('description', null, ['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Введите описание']) }}
        </div>

        <div class="form-group">
            {{ Form::label('city', 'Город', ['class' => 'label-control']) }}
            {{ Form::text('city', null, ['class' => 'form-control', 'id' => 'city', 'placeholder' => 'Введите город']) }}
        </div>
        <div class="form-group">
            {{ Form::label('phone_work', 'Рабочий телефон', ['class' => 'label-control']) }}
            {{ Form::text('phone_work', null, ['class' => 'form-control', 'id' => 'phone_work', 'placeholder' => 'Введите рабочий телефон']) }}
        </div>
        <div class="form-group">
            {{ Form::label('phone_personal', 'Личный телефон', ['class' => 'label-control']) }}
            {{ Form::text('phone_personal', null, ['class' => 'form-control', 'id' => 'phone_personal', 'placeholder' => 'Введите личный телефон']) }}
        </div>
    </div>
    <div class="form-group text-center">
            {{ Form::submit('Создать', ['class' => 'btn btn-primary']) }}
            {{ Html::tag('a', 'Назад', ['href' => route('users.index'), 'class' => 'btn btn-outline-secondary']) }}        
    </div>
    {{ Form::close() }}
@endsection