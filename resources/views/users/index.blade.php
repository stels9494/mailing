@extends('layouts.mailing')

@section('title')
    Список всех пользователей
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="form-group">
                <a class="btn btn-primary" href="{{ route('users.create') }}">Создать <i class="fa fa-plus ml-1"></i></a>
                <a class="btn btn-primary" href="{{ route('import.index') }}">Импорт <i class="fas fa-upload ml-1"></i></a>
            </div>
        </div>
        <div class="col-12 col-md-6">
            {{ Form::open(['url' => route('mailing.run'), 'onsubmit' => 'return validateMailingForm($(this))']) }}    
                <input type="hidden" name="users">            
                <div class="input-group">
                    <select name="template_id" class="custom-select" onclick="removeIsInvalidClass($(this))">
                        <option selected disabled>Выберите шаблон письма...</option>
                        @foreach ($data['templates'] as $template)
                            <option value="{{ $template->id }}">{{ $template->name }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-primary run-mailing" type="submit">Запустить</button>
                    </div>
                </div>   
            {{ Form::close() }}
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-2 mt-3 mb-3">
            <select class="number-view custom-select">
                @foreach ($data['qty'] as $one)
                    <option {{ $data['currentQty'] == $one ? 'selected' : '' }} value="{{ $one }}">{{ $one }}</option>
                @endforeach
            </select>
        </div>
    </div>

    {{ $data['users']->links() }}
    <table class="table table-striped">
        <thead>
            <th>
                <input class="custom-control users-checkbox" type="checkbox">
            </th>
            <th>#</th>
            <th>email</th>
            <th class="w-25">Название</th>
            <th class="w-25">Должность</th>
            <th class="w-25">&nbsp;</th>
        </thead>
        <tbody>
            @foreach ($data['users'] ?? [] as $user)
                <tr>
                    <td>
                        <input data-user-id="{{ $user->id }}" class="custom-control user-checkbox" type="checkbox">
                    </td>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->position }}</td>
                    <td class="text-right">
                        <a title="Редактировать" href="{{ route('users.edit', $user->id) }}"><i class="fas fa-edit"></i></a>
                        {{ Form::open(['url' => route('users.destroy', $user), 'method' => 'delete', 'onsubmit' => 'return agreeRemove("'.$user->email.'")', 'class' => 'd-inline']) }}
                            <a class="trash user-remove" title="Удалить пользователя" href="#"><i class="fas fa-trash"></i></a>
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $data['users']->links() }}

@endsection
