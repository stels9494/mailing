<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Message extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $template;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $template)
    {
        $this->user = $user;
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user->toArray();
        $keys = array_keys($user);
        $template = $this->template;
        $msg = $template->template;
        foreach ($keys as $key) {
            $msg = str_replace('{{'.$key.'}}', $user[$key], $msg);
        }
        $message = $this->to($user['email'])
                        ->subject($template->subject)
                        ->view('mail', compact('msg', 'template', 'user'));
        foreach ($template->getMedia() as $file) {
            $message->attach($file->getPath());
        }
        return $message;
    }
}
