<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class TemplateUser extends Model
{
	protected $table = 'template_user';
    public $fillable = [
    	'user_id',
    	'template_id',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function template() {
        return $this->belongsTo('App\models\Template');
    }

    public static function insertRow($user_id, $template_id) {
    	return self::create(compact('user_id', 'template_id'));
    }

    public static function selectHowViewed($user_id, $template_id) {
        $models = self::where([
            ['user_id', '=', $user_id],
            ['template_id', '=', $template_id]
        ])->get();
        foreach ($models as $model) {
            $model->viewed = true;
            $model->save();
        }
    }

    public static function getList($qty = 50) {
        return self::orderBy('id', 'desc')->paginate($qty);
    }
}
