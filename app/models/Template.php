<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Template extends Model implements HasMedia
{
    use HasMediaTrait;

    public $fillable = [
        'name',
        'template',
        'subject',
    ];

    public function getUsers() {
        return $this->belongsToMany('App\User');
    }

    /**
     * прикрепление файлов к шаблону
     */
    public function attachFiles($files) {
        foreach ($files ?? [] as $file) {
            $this->addMedia($file->path())
                ->preservingOriginal()
                ->usingFileName($file->getClientOriginalName())
                ->setName($file->getClientOriginalName())
                ->toMediaCollection();
        }
    }
}
