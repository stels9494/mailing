<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\models\TemplateUser;
use App\Jobs\RunMailing;
use App\models\Template;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * отмечает сообщение как прочитанное
     */
    public function messageViewed(Request $request) {
    	if (isset($request->template_id, $request->user_id)) {
	    	TemplateUser::selectHowViewed($request->user_id, $request->template_id);    		
    	}
        return response()->file(public_path('img/message.png'));
    }

    /**
     * запуск рассылки
     */
    public function mailingRun(Request $request) {
    	$users = json_decode($request->users, true);
		$template = Template::findOrFail($request->template_id);
        foreach ($users as $i => $user_id) {
            RunMailing::dispatch($user_id, $template)->delay(now()->addSeconds($i*config('mailing.send_interval')));            
        }
    	return redirect()->back()->with('success', 'Рассылка запущена!');
    }
}
