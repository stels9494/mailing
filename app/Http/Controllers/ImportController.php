<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Import\Store;
use App\User;

class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('import.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        // User::importFromCsv($request);
        User::importFromExcel($request);
        return back()->with('success', 'Импорт данных завершен.');
    }
}
