<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\TemplateUser;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currentQty = $request->qty ?? 50;
        $messages = TemplateUser::getList($currentQty);
        $qty = [50, 100, 200, 300, 400, 500];
        $data = compact('messages', 'qty', 'currentQty');
        return view('messages.index', compact('data'));
    }

}
