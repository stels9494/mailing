<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use \PhpOffice\PhpSpreadsheet\IOFactory;

class ApiController extends Controller
{
    private function getResponseSuccess($data = null) {
        return [
            'status' => 'ok'
        ] + ($data ? compact('data') : []);
    }

    /**
     * возвращает список заголовков
     */
    public function getHeadersExcel(Request $request) {
        $filename = $request->file->path();
        $stylesheet = IOFactory::load($filename);
        $activesheet = $stylesheet->getActiveSheet();
        $headers = [];
        for ($i = 'A'; $i <= 'Z'; $i++) {
            $value = $activesheet->getCell($i.'1')->getValue();
            if ($value === null || strlen(trim($value)) == 0) continue; 
            $headers[] = $value;
        }
        return $this->getResponseSuccess($headers);
    }

    /**
     * возвращает список полей бд users
     */
    public function getUserFields() {
        $fields = User::getDirectives();
        return $this->getResponseSuccess($fields);
    }
}
