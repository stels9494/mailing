<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Template;
use App\Http\Requests\Template\Store;
use App\Http\Requests\Template\Update;
use App\User;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currentQty = $request->qty ?? 50;
        $qty = [50, 100, 200, 300, 400, 500];
        $templates = Template::paginate($currentQty);
        $data = compact('templates', 'currentQty', 'qty');
        return view('templates.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listDirectives = User::getDirectives();
        $data = compact('listDirectives');
        return view('templates.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $template = Template::create($request->all());
        if ($template) {
            $template->attachFiles($request->file);
            return redirect()->route('templates.edit', $template);
        }
        return back()->with('error', 'Ошибка. Перепроверьте все данные.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Template $template)
    {
        return redirect()->route('templates.edit', $template);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template)
    {
        $listDirectives = User::getDirectives();
        $data = compact('template', 'listDirectives');
        return view('templates.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Template $template)
    {
        $template->update($request->all());
        $template->attachFiles($request->file);
        return back()->with('success', 'Шаблон изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Template $template)
    {
        switch ($request->action) {
            // удаление прикрепленног файла к шаблону
            case 'remove-file':
                $template->deleteMedia($request->file_id);
                return;

            // удаление шаблона
            case 'remove-template':
                $template->delete();
                return back()->with('success', 'Шаблон удален!');
        }
    }
}
