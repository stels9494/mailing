<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\Message;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\models\TemplateUser;
use React\EventLoop\Factory;
use Illuminate\Support\Facades\Log;

class RunMailing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user_id;
    private $template;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id, $template)
    {
        // Log::info('отрабатывает конструктор jobs');
        $this->user_id = $user_id;
        $this->template = $template;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user_id);
        try {
            TemplateUser::insertRow($user->id, $this->template->id);
            Mail::send(new Message($user, $this->template));
        } catch (Exception $e) {
            Log::error('Email: не удалось отправить сообщение на '.$user->email.' '.$e->getMessage());
        }
    }
}
