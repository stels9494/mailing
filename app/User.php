<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \PhpOffice\PhpSpreadsheet\IOFactory;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password', 
        'name',
        'position',
        'email',
        'company_name',
        'firstname',
        'lastname',
        'patronymic',
        'fullname',
        'postcode',
        'description',
        'city',
        'phone_work',
        'phone_personal',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // список пользователей без теущего авторизировнного
    public static function getList($qty = 50) {
        return self::where('id', '<>', auth()->user()->id)
            ->orderBy('id', 'desc')
            ->paginate($qty);
    }

    protected static $directives = [
            'name',
            'position',
            'email',
            'company_name',
            'firstname',
            'lastname',
            'patronymic',
            'fullname',
            'postcode',
            'description',
            'city',
            'phone_work',
            'phone_personal',
        ];

    public static function getDirectives() {
        return self::$directives;
    }

    public function getMessages() {
        return $this->belongsToMany('App\models\Template');
    }

    public static function importFromCsv($request) {
        $f = fopen($request->file->path(), 'r');

        // соотнести ключи
        $headers = fgetcsv($f, 0, $request->delimeter);
        $keys = [];
        foreach ($headers as $key => $head) {
            $keys[$head] = $key;
        }

        $finish_keys = [];
        foreach ($request->relations as $i => $relation) {
            $finish_keys[$relation['to']] = $keys[$relation['from']];
        }

        while ($row = fgetcsv($f, 0, $request->delimeter)) {
            $data = [];
            foreach ($finish_keys as $key => $value) {
                $data[$key] = $row[$value];
            }
            // dd($data);
            $user = User::where('email', $data['email'])->first();
            if ($user) {
                if ($request->replace) {
                    $user->update($data);
                }
            }else{
                User::create($data);
            }
        }
        fclose($f);
    }

    public static function importFromExcel($request) {
        $filename = $request->file->path();
        $spreadsheet = IOFactory::load($filename);
        $activesheet = $spreadsheet->getActiveSheet();

        $headers = [];
        for ($i = 'A'; $i <= 'Z'; $i++) {
            $value = $activesheet->getCell($i.'1')->getValue();
            if ($value === null || !strlen(trim($value))) continue;
            $headers[$i] = $value;
        }

        $keys = [];
        foreach ($headers as $key => $head) {
            $keys[$head] = $key;
        }
        $finish_keys = [];
        foreach ($request->relations as $i => $relation) {
            $finish_keys[$keys[$relation['from']]] = $relation['to'];
        }
        $maxNumRows = $activesheet->getHighestDataRow();
        $maxNumColumns = $activesheet->getHighestDataColumn();
        for ($i = 1; $i <= $maxNumRows; $i++) {
            $data = [];

            for ($j = 'A'; $j <= $maxNumColumns; $j++) {
                if (isset($finish_keys[$j])) {
                    $data[$finish_keys[$j]] = $activesheet->getCell($j.$i)->getValue();
                }
            }

            // если не задан email - не создавать запись в users
            if (!isset($data['email'])) {
                continue;
            }

            if (!self::isValidEmail($data['email'])) {
                // если там что-то есть - пытаемся разбить на email-ы
                if (strlen(trim($data['email'])) > 0) {
                    $emails = explode(',', $data['email']);
                    foreach ($emails as $email) {
                        $email = trim($email);
                        if (self::isValidEmail($email)) {
                            self::createOrUpdate(array_merge($data, compact('email')));
                        }
                    }
                }
                continue;
            } else self::createOrUpdate($data);
        }
    }

    public static function createOrUpdate($data) {
        $user = User::where('email', $data['email'])->first();
        // если пользователь найден и флаг - перезаписать данные
        if ($user && request()->replace) {
            $user->update($data);
        }
        // если пользователь не найден - создать
        if (!$user) {
            $user = User::create($data);
        }
        return $user;
    }

    public static function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
